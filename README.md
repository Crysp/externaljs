#Browser Support

* IE 9+
* Edge 12+
* Firefox 40+
* Chrome 43+
* Safari 8+
* Opera 32+
* iOS Safari 8.4+
* Opera Mini 8
* Android Browser 4.1+
* Chrome for Android 46
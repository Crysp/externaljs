
var gulp = require('gulp');

var runSequence = require('run-sequence').use(gulp);
var rigger = require('gulp-rigger');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('js', function () {
    return gulp.src('lib/external.js')
        .pipe(rigger())
        .pipe(gulp.dest('dist'));
});

gulp.task('js-min-ver', function () {
    return gulp.src('dist/external.js')
        .pipe(uglify())
        .pipe(rename('external.min.js'))
        .pipe(gulp.dest('dist'));
});

gulp.task('watch', function () {

    gulp.watch('lib/**/*.js', function () {
        runSequence('js', 'js-min-ver');
    });

});

gulp.task('default', function () {

    runSequence('js', 'js-min-ver');

});
//= copyright

(function (root, factory) {

    if (typeof module !== 'undefined' && module.exports) {
        module.exports = factory();
    } else if (typeof define === 'function' && define.amd) {
        define(factory);
    } else {
        root.ExternalJS = factory();
    }

})(this, function () {

    var doc = document;
    var head = doc.getElementsByTagName('head')[0];
    // кеш скриптов
    //= _cache

    /**
     * Цикл для массива с колбекем на каждом элементе
     *
     * @param {Array} array - массив
     * @param {Function} itemCallback - колбек для элемента массива
     * @returns {boolean}
     */
    function each(array, itemCallback) {

        if (typeof itemCallback === 'undefined') {
            return false;
        }

        for (var i = 0, length = array.length; i < length; i++) {
            itemCallback.call(array, array[i], i);
        }

        return true;
    }

    /**
     * Добавляет тег script в DOM
     * @param {String} url - ссылка на скрипт
     */
    function insertScriptTag(url) {

        var scriptTag = doc.createElement('script');

        scriptTag.async = true;
        scriptTag.src = url;

        scriptTag.onload = function () {

            ScriptCache.setReady(url);

            // выполняются все удачные колбеки, хранящиеся в стеке
            var callStack = ScriptCache.get(url).callStack;
            for (var i = 0, length = callStack.length; i < length; i++) {
                callStack[i].onLoad();
            }

            ScriptCache.clearCallStack(url);
        };

        scriptTag.onerror = function () {

            // выполняются все неудачные колбеки, хранящиеся в стеке
            var callStack = ScriptCache.get(url).callStack;
            for (var i = 0, length = callStack.length; i < length; i++) {
                callStack[i].onError();
            }

            // не подгрузился, скрипт удаляется, чтобы дать ему второй шанс
            ScriptCache.clearCallStack(url);
            ScriptCache.remove(url);

            head.removeChild(scriptTag);
        };

        head.insertBefore(scriptTag, head.lastChild);
    }

    /**
     * Основной метод модуля, подключающий сторонние скрипты
     *
     * @param {String|Array} requirements - ссылка на скрипт или массив из ссылок на скрипты
     * @param {Object} options - список параметров (пока хранит колбеки)
     * @returns {ExternalJS}
     * @constructor
     */
    function ExternalJS(requirements, options) {

        var libs = (requirements instanceof Array) ? requirements : [requirements];

        each(libs, function (lib) {

            if (!ScriptCache.has(lib)) {
                // пускай в кеше полежит пока грузится
                ScriptCache.add(lib);
                ScriptCache.addCallback(lib, options);
                insertScriptTag(lib);
                return;
            } else if (!ScriptCache.isReady(lib)) {
                // т.к. скрипт еще не готов значит он грузится и надо колбеки пихнуть в стек вызова
                ScriptCache.addCallback(lib, options);
                return;
            }

            // если скрипт в кеше, значит он подгрузился и можно спокойно выполнить удачный колбек
            options.onLoad();
        });

        return ExternalJS;
    }

    return ExternalJS;

});
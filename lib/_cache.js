
var ScriptCache = {

    store: [],

    /**
     * Добавляет скрипт в кеш, но с флагом ready: false (значит что скрипт пока грузится)
     * Колбеки хранятся в стеке и будут вызываться по очереди после готовности скрипта
     *
     * @param {String} url - ссылка на скрипт
     */
    add: function (url) {
        this.store.push({
            url: url,
            callStack: [],
            ready: false
        });
    },

    /**
     * Добавляет колбеки для скрипта
     *
     * @param url
     * @param callbacks
     */
    addCallback: function (url, callbacks) {
        this.get(url).callStack.push(callbacks);
    },

    /**
     * Очищает стек вызова для скрипта
     *
     * @param {String} url - ссылка на скрипт
     */
    clearCallStack: function (url) {
        this.get(url).callStack = [];
    },

    /**
     * Удаляет скрипт из кеша
     *
     * @param {String} url - ссылка на скрипт
     */
    remove: function (url) {
        this.store.splice(this.store.indexOf(url), 1);
    },

    /**
     * Достает кеш скрипта по ссылке
     *
     * @param {String} url - ссылка на скрипт
     * @returns {Object|undefined}
     */
    get: function (url) {
        for (var i = 0, length = this.store.length; i < length; i++) {
            if (this.store[i].url === url) {
                return this.store[i];
            }
        }
    },

    /**
     * Скрипт готов к использованию
     *
     * @param {String} url - ссылка на скрипт
     */
    setReady: function (url) {
        this.get(url).ready = true;
    },

    /**
     * Проверяет есть ли скрипт в кеше
     *
     * @param {String} url - ссылка на скрипт
     * @returns {boolean}
     */
    has: function (url) {
        return typeof this.get(url) !== 'undefined';
    },

    /**
     * Скрипт загрузился
     *
     * @param {String} url - ссылка на скрипт
     * @returns {boolean}
     */
    isReady: function (url) {
        return this.get(url).ready;
    }
};